package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.Quote;

@Getter
@Setter

public class QuoteResponse {

    private Long id;
    private String text;

    private AuthorResponse authorResponse;

    public QuoteResponse(Quote quote) {
        id = quote.getId();
        text = quote.getText();
        authorResponse = new AuthorResponse(quote.getAuthor());
    }
}
