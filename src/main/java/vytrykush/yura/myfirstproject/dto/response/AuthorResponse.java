package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.Author;

@Getter
@Setter
@NoArgsConstructor

public class AuthorResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String pathToImage;

    public AuthorResponse(Author author) {
        id = author.getId();
        firstName = author.getFirstName();
        lastName = author.getLastName();
        pathToImage = author.getPathToImage();
    }
}
