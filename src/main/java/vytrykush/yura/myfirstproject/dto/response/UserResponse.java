package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.User;

@Getter
@Setter

public class UserResponse {

    private Long id;
    private String username;

    public UserResponse(User user) {
        id = user.getId();
        username = user.getUsername();
    }
}
