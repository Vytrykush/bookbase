package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.Genre;

@Getter
@Setter

public class GenreResponse {

    private Long id;
    private String name;

    public GenreResponse(Genre genre) {
        id = genre.getId();
        name = genre.getName();
    }
}
