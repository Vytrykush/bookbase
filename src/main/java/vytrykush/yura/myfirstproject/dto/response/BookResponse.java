package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.Book;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter

public class BookResponse {

    private Long id;
    private String title;
    private Integer firstPublished;
    private String pathToImage;

    private List<AuthorResponse> authorResponses;
    private List<GenreResponse> genreResponses;

    public BookResponse(Book book) {
        id = book.getId();
        title = book.getTitle();
        firstPublished = book.getFirstPublished();
        pathToImage = book.getPathToImage();
        authorResponses = book.getAuthors().stream().map(AuthorResponse::new).collect(Collectors.toList());
        genreResponses = book.getGenres().stream().map(GenreResponse::new).collect(Collectors.toList());
    }
}
