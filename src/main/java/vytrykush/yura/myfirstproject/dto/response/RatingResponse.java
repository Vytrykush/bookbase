package vytrykush.yura.myfirstproject.dto.response;

import lombok.Getter;
import lombok.Setter;
import vytrykush.yura.myfirstproject.entity.Rating;

@Getter
@Setter

public class RatingResponse {

    private Long id;
    private Double mark;

    private UserResponse userResponse;

    public RatingResponse(Rating rating) {
        id = rating.getId();
        mark = rating.getMark();
        userResponse = new UserResponse(rating.getUser());
    }
}
