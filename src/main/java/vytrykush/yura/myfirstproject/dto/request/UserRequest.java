package vytrykush.yura.myfirstproject.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter

public class UserRequest {

    @NotNull
    @Size(min = 4, max = 20)
    private String username;
}
