package vytrykush.yura.myfirstproject.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.NotNull;

@Getter
@Setter

public class PaginationRequest {

    @NotNull
    private Integer page;
    @NotNull
    private Integer size;
    private String fieldName;
    private Sort.Direction direction;

    public Pageable toPageable() {
        if (fieldName == null || direction == null) {
            return PageRequest.of(page, size);
        } else {
            return PageRequest.of(page, size, direction, fieldName);
        }
    }
}
