package vytrykush.yura.myfirstproject.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter

public class BookRequest {

    @NotNull
    private String title;
    private Integer firstPublished;
    private String pathToImage;
    private FileRequest fileRequest;

    @NotNull
    private List<Long> authorId;
    @NotNull
    private List<Long> genreId;

}
