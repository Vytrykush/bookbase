package vytrykush.yura.myfirstproject.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter

public class QuoteRequest {

    @NotNull
    private String text;
    private Long authorId;
}
