package vytrykush.yura.myfirstproject.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter

public class RatingRequest {

    @NotNull
    private Double mark;
    private Long userId;
}
