package vytrykush.yura.myfirstproject.exception;

public class WrongInputDataException extends RuntimeException {

    public WrongInputDataException() {
    }

    public WrongInputDataException(String message) {
        super(message);
    }
}
