package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.UserRequest;
import vytrykush.yura.myfirstproject.dto.response.UserResponse;
import vytrykush.yura.myfirstproject.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public UserResponse save(@RequestBody UserRequest userRequest) {
        return userService.save(userRequest);
    }

    @GetMapping
    public List<UserResponse> findAll() {
        return userService.findAll();
    }

    @PutMapping
    public UserResponse update(@RequestParam Long id, @RequestBody UserRequest userRequest) {
        return userService.update(userRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        userService.delete(id);
    }

}
