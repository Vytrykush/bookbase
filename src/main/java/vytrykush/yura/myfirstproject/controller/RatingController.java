package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.RatingRequest;
import vytrykush.yura.myfirstproject.dto.response.RatingResponse;
import vytrykush.yura.myfirstproject.service.RatingService;

import java.util.List;

@RestController
@RequestMapping("/rating")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @PostMapping
    public RatingResponse save(@RequestBody RatingRequest ratingRequest) {
        return ratingService.save(ratingRequest);
    }

    @GetMapping
    public List<RatingResponse> findAll() {
        return ratingService.findAll();
    }

    @PutMapping
    public RatingResponse update(@RequestParam Long id, @RequestBody RatingRequest ratingRequest) {
        return ratingService.update(ratingRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        ratingService.delete(id);
    }

}
