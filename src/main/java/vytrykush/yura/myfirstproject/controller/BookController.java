package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.BookRequest;
import vytrykush.yura.myfirstproject.dto.request.PaginationRequest;
import vytrykush.yura.myfirstproject.dto.response.BookResponse;
import vytrykush.yura.myfirstproject.dto.response.DataResponse;
import vytrykush.yura.myfirstproject.service.BookService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping
    public BookResponse save(@RequestBody BookRequest bookRequest) throws IOException {
        return bookService.save(bookRequest);
    }

    @PostMapping("/page")
    public DataResponse<BookResponse> findPage(@Valid @RequestBody PaginationRequest paginationRequest) {
        return bookService.findPage(paginationRequest);
    }

    @GetMapping("/filter")
    public DataResponse<BookResponse> filter(@RequestParam(required = false) String title,
                                             @RequestParam(required = false) Integer firstPublished,
                                             @RequestParam Integer page,
                                             @RequestParam Integer size,
                                             @RequestParam String fieldName,
                                             @RequestParam Sort.Direction direction) {
        return bookService.getPageByFilter(title, firstPublished, page, size, fieldName, direction);
    }

    @GetMapping
    public List<BookResponse> findAll() {
        return bookService.findAll();
    }

    @PutMapping
    public BookResponse update(@RequestParam Long id, @RequestBody BookRequest bookRequest) throws IOException {
        return bookService.update(bookRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        bookService.delete(id);
    }

}
