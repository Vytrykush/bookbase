package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.GenreRequest;
import vytrykush.yura.myfirstproject.dto.response.GenreResponse;
import vytrykush.yura.myfirstproject.service.GenreService;

import java.util.List;

@RestController
@RequestMapping("/genre")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @PostMapping
    public GenreResponse save(@RequestBody GenreRequest genreRequest) {
        return genreService.save(genreRequest);
    }

    @GetMapping
    public List<GenreResponse> findAll() {
        return genreService.findAll();
    }

    @PutMapping
    public GenreResponse update(@RequestParam Long id, @RequestBody GenreRequest genreRequest) {
        return genreService.update(genreRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        genreService.delete(id);
    }

}
