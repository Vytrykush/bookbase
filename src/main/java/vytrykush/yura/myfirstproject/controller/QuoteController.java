package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.QuoteRequest;
import vytrykush.yura.myfirstproject.dto.response.QuoteResponse;
import vytrykush.yura.myfirstproject.service.QuoteService;

import java.util.List;

@RestController
@RequestMapping("/quote")
public class QuoteController {

    @Autowired
    private QuoteService quoteService;

    @PostMapping
    public QuoteResponse save(@RequestBody QuoteRequest quoteRequest) {
        return quoteService.save(quoteRequest);
    }

    @GetMapping
    public List<QuoteResponse> findAll() {
        return quoteService.findAll();
    }

    @PutMapping
    public QuoteResponse update(@RequestParam Long id, @RequestBody QuoteRequest quoteRequest) {
        return quoteService.update(quoteRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        quoteService.delete(id);
    }

}
