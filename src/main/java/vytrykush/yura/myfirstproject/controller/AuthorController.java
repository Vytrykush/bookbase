package vytrykush.yura.myfirstproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import vytrykush.yura.myfirstproject.dto.request.AuthorRequest;
import vytrykush.yura.myfirstproject.dto.request.PaginationRequest;
import vytrykush.yura.myfirstproject.dto.response.AuthorResponse;
import vytrykush.yura.myfirstproject.dto.response.DataResponse;
import vytrykush.yura.myfirstproject.service.AuthorService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @PostMapping
    public AuthorResponse save(@RequestBody AuthorRequest authorRequest) throws IOException {
        return authorService.save(authorRequest);
    }

    @PostMapping("/page")
    public DataResponse<AuthorResponse> findPage(@Valid @RequestBody PaginationRequest paginationRequest) {
        return authorService.findPage(paginationRequest);
    }

    @GetMapping("/filter")
    public DataResponse<AuthorResponse> filter(@RequestParam(required = false) String firstName,
                                               @RequestParam(required = false) String lastName,
                                               @RequestParam Integer page,
                                               @RequestParam Integer size,
                                               @RequestParam String fieldName,
                                               @RequestParam Sort.Direction direction) {
        return authorService.getPageByFilter(firstName, lastName, page, size, fieldName, direction);
    }

    @GetMapping
    public List<AuthorResponse> findAll() {
        return authorService.findAll();
    }

    @PutMapping
    public AuthorResponse update(@RequestParam Long id, @RequestBody AuthorRequest authorRequest) throws IOException {
        return authorService.update(authorRequest, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) {
        authorService.delete(id);
    }

}