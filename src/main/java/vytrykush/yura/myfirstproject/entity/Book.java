package vytrykush.yura.myfirstproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private Integer firstPublished; // what year
    private String pathToImage;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Author> authors;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Genre> genres;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<Rating> ratings;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", firstPublished=" + firstPublished +
                '}';
    }
}
