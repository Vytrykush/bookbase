package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.RatingRequest;
import vytrykush.yura.myfirstproject.dto.response.RatingResponse;
import vytrykush.yura.myfirstproject.entity.Rating;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.RatingRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    public RatingResponse save(RatingRequest ratingRequest) {
        return new RatingResponse(ratingRepository.save(ratingRequestToRating(ratingRequest, null)));
    }

    public List<RatingResponse> findAll() {
        return ratingRepository.findAll().stream()
                .map(RatingResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        ratingRepository.delete(findOne(id));
    }

    public RatingResponse update(RatingRequest ratingRequest, Long id) {
        Rating rating = ratingRequestToRating(ratingRequest, findOne(id));
        return new RatingResponse(ratingRepository.save(rating));
    }

    public Rating findOne(Long id) {
        return ratingRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Rating with id " + id + " not exist"));
    }

    private Rating ratingRequestToRating(RatingRequest ratingRequest, Rating rating) {
        if (rating == null) {
            rating = new Rating();
        }
        rating.setMark(ratingRequest.getMark());
        return rating;
    }
}

