package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.QuoteRequest;
import vytrykush.yura.myfirstproject.dto.response.QuoteResponse;
import vytrykush.yura.myfirstproject.entity.Author;
import vytrykush.yura.myfirstproject.entity.Quote;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.QuoteRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuoteService {

    @Autowired
    private QuoteRepository quoteRepository;

    @Autowired
    private AuthorService authorService;

    public QuoteResponse save(QuoteRequest quoteRequest) {
        return new QuoteResponse(quoteRepository.save(quoteRequestToQuote(quoteRequest, null)));
    }

    public List<QuoteResponse> findAll() {
        return quoteRepository.findAll().stream()
                .map(QuoteResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        quoteRepository.delete(findOne(id));
    }

    public QuoteResponse update(QuoteRequest quoteRequest, Long id) {
        Quote quote = quoteRequestToQuote(quoteRequest, findOne(id));
        return new QuoteResponse(quoteRepository.save(quote));
    }

    public Quote findOne(Long id) {
        return quoteRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Author with id " + id + " not exist"));
    }

    private Quote quoteRequestToQuote(QuoteRequest quoteRequest, Quote quote) {
        if (quote == null) {
            quote = new Quote();
        }
        quote.setText(quoteRequest.getText());

        Author author = authorService.findOne(quoteRequest.getAuthorId());
        quote.setAuthor(author);
        return quote;
    }

}
