package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.AuthorRequest;
import vytrykush.yura.myfirstproject.dto.request.PaginationRequest;
import vytrykush.yura.myfirstproject.dto.response.AuthorResponse;
import vytrykush.yura.myfirstproject.dto.response.DataResponse;
import vytrykush.yura.myfirstproject.entity.Author;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.AuthorRepository;
import vytrykush.yura.myfirstproject.specification.AuthorSpecification;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private FileService fileService;

    public AuthorResponse save(AuthorRequest authorRequest) throws IOException {
        Author author = new Author();
        author.setFirstName(authorRequest.getFirstName());
        author.setLastName(authorRequest.getLastName());
        String file = fileService.saveFile(authorRequest.getPathToImage());
        author.setPathToImage(file);
        Author save = authorRepository.save(author);
        return new AuthorResponse(save);
    }

    public List<AuthorResponse> findAll() {
        return authorRepository.findAll().stream()
                .map(AuthorResponse::new)
                .collect(Collectors.toList());
    }

    public DataResponse<AuthorResponse> findPage(PaginationRequest paginationRequest) {
        Page<Author> page = authorRepository.findAll(paginationRequest.toPageable());
        List<AuthorResponse> collect = page.get().map(AuthorResponse::new).collect(Collectors.toList());
        return new DataResponse<>(page.getTotalPages(), page.getTotalElements(), collect);
    }

    public void delete(Long id) {
        authorRepository.delete(findOne(id));
    }

    public AuthorResponse update(AuthorRequest authorRequest, Long id) throws IOException {
        Author author = authorRequestToAuthor(authorRequest, findOne(id));
        return new AuthorResponse(authorRepository.save(author));
    }

    public Author findOne(Long id) {
        return authorRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Author with id " + id + " not exist"));
    }

    private Author authorRequestToAuthor(AuthorRequest authorRequest, Author author) throws IOException {
        if (author == null) {
            author = new Author();
            author = authorRepository.save(author);
        }
        author.setFirstName(authorRequest.getFirstName());
        author.setLastName(authorRequest.getLastName());
        return author;
    }

    public DataResponse<AuthorResponse> getPageByFilter(String firstName, String lastName, Integer page, Integer size, String fieldName, Sort.Direction direction) {
        Page<Author> all = authorRepository.findAll(new AuthorSpecification(firstName, lastName), PageRequest.of(page, size, direction, fieldName));
        List<AuthorResponse> collect = all.get().map(AuthorResponse::new).collect(Collectors.toList());
        return new DataResponse<>(all.getTotalPages(), all.getTotalElements(), collect);
    }
}
