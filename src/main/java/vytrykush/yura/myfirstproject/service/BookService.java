package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.BookRequest;
import vytrykush.yura.myfirstproject.dto.request.PaginationRequest;
import vytrykush.yura.myfirstproject.dto.response.BookResponse;
import vytrykush.yura.myfirstproject.dto.response.DataResponse;
import vytrykush.yura.myfirstproject.entity.Author;
import vytrykush.yura.myfirstproject.entity.Book;
import vytrykush.yura.myfirstproject.entity.Genre;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.BookRepository;
import vytrykush.yura.myfirstproject.specification.BookSpecification;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private GenreService genreService;

    @Autowired
    private FileService fileService;

    public BookResponse save(BookRequest bookRequest) throws IOException {
        return new BookResponse(bookRepository.save(bookRequestToBook(bookRequest, null)));
    }

    public List<BookResponse> findAll() {
        return bookRepository.findAll().stream()
                .map(BookResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        bookRepository.delete(findOne(id));
    }

    public BookResponse update(BookRequest bookRequest, Long id) throws IOException {
        Book book = bookRequestToBook(bookRequest, findOne(id));
        return new BookResponse(bookRepository.save(book));
    }

    public Book findOne(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Book with id " + id + " not exist"));
    }

    public DataResponse<BookResponse> findPage(PaginationRequest paginationRequest) {
        Page<Book> page = bookRepository.findAll(paginationRequest.toPageable());
        List<BookResponse> collect = page.get().map(BookResponse::new).collect(Collectors.toList());
        return new DataResponse<>(page.getTotalPages(), page.getTotalElements(), collect);
    }

    private Book bookRequestToBook(BookRequest bookRequest, Book book) throws IOException {
        if (book == null) {
            book = new Book();
            book = bookRepository.save(book);
        }
        book.setTitle(bookRequest.getTitle());
        book.setFirstPublished(bookRequest.getFirstPublished());
        String file = fileService.saveFile(bookRequest.getPathToImage());
        book.setPathToImage(file);
        List<Author> collect = bookRequest.getAuthorId().stream().map(authorService::findOne).collect(Collectors.toList());
        book.setAuthors(collect);
        List<Genre> collect1 = bookRequest.getGenreId().stream().map(genreService::findOne).collect(Collectors.toList());
        book.setGenres(collect1);
        return book;
    }

    public DataResponse<BookResponse> getPageByFilter(String title, Integer firstPublished, Integer page, Integer size, String fieldName, Sort.Direction direction) {
        Page<Book> all = bookRepository.findAll(new BookSpecification(title, firstPublished),
                PageRequest.of(page, size, direction, fieldName)
        );
        List<BookResponse> collect = all.get().map(BookResponse::new).collect(Collectors.toList());
        return new DataResponse<>(all.getTotalPages(), all.getTotalElements(), collect);
    }
}
