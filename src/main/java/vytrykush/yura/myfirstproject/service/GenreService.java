package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.GenreRequest;
import vytrykush.yura.myfirstproject.dto.response.GenreResponse;
import vytrykush.yura.myfirstproject.entity.Genre;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.GenreRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    public GenreResponse save(GenreRequest genreRequest) {
        Genre genre = new Genre();
        genre.setName(genreRequest.getName());
        Genre save = genreRepository.save(genre);
        return new GenreResponse(save);
    }

    public List<GenreResponse> findAll() {
        return genreRepository.findAll().stream().map(GenreResponse::new).collect(Collectors.toList());
    }

    public void delete(Long id) {
        genreRepository.delete(findOne(id));
    }

    public GenreResponse update(GenreRequest genreRequest, Long id) {
        Genre genre = genreRequestToGenre(genreRequest, findOne(id));
        return new GenreResponse(genreRepository.save(genre));
    }

    public Genre findOne(Long id) {
        return genreRepository.findById(id).orElseThrow(() -> new WrongInputDataException("Author with id " + id + " not exist"));
    }

    private Genre genreRequestToGenre(GenreRequest genreRequest, Genre genre) {
        if (genre == null) {
            genre = new Genre();
        }
        genre.setName(genreRequest.getName());
        return genre;
    }
}
