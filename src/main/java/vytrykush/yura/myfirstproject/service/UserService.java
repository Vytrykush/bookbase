package vytrykush.yura.myfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vytrykush.yura.myfirstproject.dto.request.UserRequest;
import vytrykush.yura.myfirstproject.dto.response.UserResponse;
import vytrykush.yura.myfirstproject.entity.User;
import vytrykush.yura.myfirstproject.exception.WrongInputDataException;
import vytrykush.yura.myfirstproject.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserResponse save(UserRequest userRequest) {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        User save = userRepository.save(user);
        return new UserResponse(save);
    }

    public List<UserResponse> findAll() {
        return userRepository.findAll().stream()
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        userRepository.delete(findOne(id));
    }

    public UserResponse update(UserRequest userRequest, Long id) {
        User user = userRequestToUser(userRequest, findOne(id));
        return new UserResponse(userRepository.save(user));
    }

    public User findOne(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new WrongInputDataException("User with id " + id + " not exist"));
    }

    private User userRequestToUser(UserRequest userRequest, User user) {
        if (user == null) {
            user = new User();
        }
        user.setUsername(userRequest.getUsername());
        return user;
    }
}
