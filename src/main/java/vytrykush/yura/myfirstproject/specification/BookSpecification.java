package vytrykush.yura.myfirstproject.specification;

import org.springframework.data.jpa.domain.Specification;
import vytrykush.yura.myfirstproject.entity.Book;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class BookSpecification implements Specification<Book> {

    private String title;
    private Integer firstPublished;

    public BookSpecification(String title, Integer firstPublished) {
        this.title = title;
        this.firstPublished = firstPublished;
    }

    @Override
    public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(byTitle(root, criteriaBuilder));
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate byTitle(Root<Book> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(root.get("title"), "%" + title + "%");
    }

//    private Predicate byFirstPublished(Root<Book> root, CriteriaBuilder criteriaBuilder) {
//        return criteriaBuilder.between(root.get("firstPublished"), firstPublished);
//    }
//    if i want i can make some another specefication
}
