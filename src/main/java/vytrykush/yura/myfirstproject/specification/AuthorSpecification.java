package vytrykush.yura.myfirstproject.specification;

import org.springframework.data.jpa.domain.Specification;
import vytrykush.yura.myfirstproject.entity.Author;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AuthorSpecification implements Specification<Author> {

    private String firstName;
    private String lastName;

    public AuthorSpecification(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public Predicate toPredicate(Root<Author> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(byFirstName(root, criteriaBuilder));
        predicates.add(byLastName(root, criteriaBuilder));
        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

    private Predicate byFirstName(Root<Author> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(root.get("firstName"), "%" + firstName + "%");
    }

    private Predicate byLastName(Root<Author> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(root.get("lastName"), "%" + lastName + "%");
    }
}
