package vytrykush.yura.myfirstproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vytrykush.yura.myfirstproject.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
