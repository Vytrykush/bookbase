package vytrykush.yura.myfirstproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vytrykush.yura.myfirstproject.entity.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
