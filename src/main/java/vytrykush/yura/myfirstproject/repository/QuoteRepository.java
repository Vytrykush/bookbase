package vytrykush.yura.myfirstproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vytrykush.yura.myfirstproject.entity.Quote;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {

}
